let themeBtn = document.querySelector('.theme-button')
themeBtn.onclick= () => {
    document.body.classList.toggle("light");
    document.body.classList.toggle("dark");
    localStorage.theme = document.body.className || "light"
}
if (!localStorage.theme) localStorage.theme = "light";
document.body.className = localStorage.theme ;
