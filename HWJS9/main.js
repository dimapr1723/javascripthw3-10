let tab = function () {
    let tabNav = document.querySelectorAll('.tabs-title'),
        tabContent = document.querySelectorAll('.tab-text'),
        tabName;


    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav);
    });

    function selectTabNav() {
        document.querySelector('.active').classList.remove('active');
        this.classList.add('active');
        tabName = this.getAttribute('data-tab');
        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        })
    }


};

tab();