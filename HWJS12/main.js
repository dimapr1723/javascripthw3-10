const image = document.querySelectorAll("img");
let showImage = 0;
let sliderMove = false;

function slider() {
    image[showImage].className = "image-to-show";
    showImage = (showImage + 1) % image.length;
    image[showImage].classList.add("show");
}

let sliderShow = setInterval(slider, 3000, (sliderMove = true));

const stop = document.querySelector(".stop");
stop.addEventListener("click", pause);

function pause() {
    clearInterval(sliderShow);
    sliderMove = false;
}

const listGo = document.querySelector(".start");
listGo.addEventListener("click", next);

function next() {
    if (!sliderMove) {
        sliderShow = setInterval(slider, 3000, (sliderMove = true));
    }
}


