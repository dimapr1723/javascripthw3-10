let input = document.getElementById('priseInput');
let warning = document.createElement('p');
warning.classList.add('p');
let button = document.createElement('span');
button.classList.add('span');
input.addEventListener('focus', () => {
    input.style.border = "1px solid green";
});
input.addEventListener('blur', () => {
    if (input.value < 0 || input.value === "") {
        input.style.border = "1px solid red";
        document.querySelector('label').append(warning);
        warning.innerText = 'Please enter correct price';
        warning.style.color = 'red'
        button.remove();
    } else {
        document.querySelector('label').prepend(button);
        button.innerHTML = `Текущая цена: ${input.value} <button class="endButton">X</button>`
        input.style.color = 'green'
        button.style.color = 'green';
        warning.remove();
        let endButton = document.querySelector('.button');
        endButton.addEventListener('click', () => {
            endButton.remove();
            input.value = "";
        });
    }
})
