let eyeIcon = document.querySelectorAll('.icon-password');
eyeIcon.forEach(item => {
    item.addEventListener('click', function (event){
        if (event.target.classList.contains('fa-eye')){
            event.target.classList.replace('fa-eye', 'fa-eye-slash')
        } else {
            event.target.classList.replace( 'fa-eye-slash', 'fa-eye')
        }
    })
});

let enterPassword = document.getElementById('firstPassword');
let confirmPassword = document.getElementById('secondPassword');
let warning = document.createElement('p');
warning.classList.add('warning_styles');

let btn = document.querySelector('.btn')
btn.addEventListener('click', function (event){
    event.preventDefault();
    if (enterPassword.value !== confirmPassword.value){
        confirmPassword.style.border = '2px solid red';
        document.getElementById('appendLabel').append(warning);
        warning.innerText = 'Please enter correct password';
        warning.style.color = 'red'
    }
    if (confirmPassword.value === enterPassword.value){
        alert('You are welcome');
    }
});

function password() {
    if (enterPassword.type === "password"){
        enterPassword.type = "text";
    }else {
        enterPassword.type = "password";
    }
}

function corrector() {
    if (confirmPassword.type === "password") {
        confirmPassword.type = "text";
    } else {
        confirmPassword.type = "password";
    }
}
