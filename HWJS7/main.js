

function renderList(items,parent){

    const elements = items.map(items => {
        const liTag = document.createElement('li');
        liTag.textContent = items;

        return liTag;
    })


    const ulTag = document.createElement('ul');
    ulTag.append(...elements);

    if(parent){
        parent.append(ulTag);
    }else {
        document.body.append(ulTag);
    }

}

renderList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])