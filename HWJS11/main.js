let key = document.querySelectorAll('.btn');
document.addEventListener('keypress', function(enter){
    key.forEach((item) => {
        if (enter.key.toUpperCase() === item.dataset.key.toUpperCase()){
            item.classList.add('key_color');
        } else {
            item.classList.remove('key_color');
        }
    });
} );